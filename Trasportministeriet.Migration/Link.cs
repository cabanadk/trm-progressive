﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trasportministeriet.Migration
{
    public class Link
    {
        public string Title { get; set; }
        public string Url { get; set; }
    }
}
