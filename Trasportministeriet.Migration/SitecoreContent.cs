﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trasportministeriet.Migration
{
    public class SitecoreContent
    {
        public SitecoreContent()
        {
            //MediaMap.Add("", "");

        }
        public Dictionary<string, string> MediaMap;

        public string Title { get; set; }
        public string Summary { get; set; }
        public string Image { get; set; }
        public DateTime Date { get; set; }
        public string ImageByline { get; set; }
        public string Body { get; set; }
        public string Media { get; set; }
        //public Link ExternalLink { get; set; }
        public List<string> InternalFileLinks { get; set; }
        //public string Category { get; set; }
    }
}
