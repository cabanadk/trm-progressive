﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using umbraco.BusinessLogic;
using YamlDotNet.RepresentationModel;

namespace Trasportministeriet.Migration
{
    public class YamlParser
    {
        public Dictionary<string, List<SitecoreContent>> GetContents(string yamlFolderSrc)
        {
            var contents = new Dictionary<string, List<SitecoreContent>>();
            var subFolders = Directory.GetDirectories(yamlFolderSrc);
            foreach (var folder in subFolders)
            {
                var folderName = Path.GetFileName(folder);

                //Only process folders with year name
                if (folderName.Length != 4)
                {
                    break;
                }

                var yamlFiles = Directory.GetFiles(folder);
                var parsedContent = new List<SitecoreContent>();

                foreach (var yamlFile in yamlFiles)
                {
                    if (Path.GetExtension(yamlFile) != ".yml")
                    {
                        break;
                    }

                    try
                    {
                        var content = Parse(yamlFile);
                        parsedContent.Add(content);
                    }
                    catch(Exception ex)
                    {
                        Log.Add(LogTypes.Error, -1, $"{yamlFile}: {ex.Message}");
                    }
                }

                contents.Add(folderName, parsedContent);
            }

            return contents;

        }

        public SitecoreContent Parse(string ymlPath)
        {
            var result = new SitecoreContent();
            var ymlStr = System.IO.File.ReadAllText(ymlPath);
            var input = new StringReader(ymlStr);

            var yaml = new YamlStream();
            yaml.Load(input);
            var mapping = (YamlMappingNode)yaml.Documents[0].RootNode;

            var languages = (YamlSequenceNode)mapping.Children[new YamlScalarNode("Languages")];
            var language = (YamlMappingNode)languages[0];
            var versions = (YamlSequenceNode)language.Children[new YamlScalarNode("Versions")];
            var version = (YamlMappingNode)versions[0];
            var fields = (YamlSequenceNode)version.Children[new YamlScalarNode("Fields")];

            foreach (YamlMappingNode field in fields)
            {
                var hint = field.Children[new YamlScalarNode("Hint")].ToString();
                if (hint.Equals("Title", StringComparison.InvariantCultureIgnoreCase))
                {
                    result.Title = field.Children[new YamlScalarNode("Value")].ToString();
                }
                else if (hint.Equals("Summary", StringComparison.InvariantCultureIgnoreCase))
                {
                    result.Summary = field.Children[new YamlScalarNode("Value")].ToString();
                }
                else if (hint.Equals("Date", StringComparison.InvariantCultureIgnoreCase))
                {
                    var dateStr = field.Children[new YamlScalarNode("Value")].ToString();
                    var year = dateStr.Substring(0, 4);
                    var month = dateStr.Substring(4, 2);
                    var day = dateStr.Substring(6, 2);
                    //var hour = dateStr.Substring(9, 2);
                    //var min = dateStr.Substring(11, 2);

                    result.Date = DateTime.Parse($"{year}-{month}-{day} 00:00:00");
                }
                else if (hint.Equals("Body", StringComparison.InvariantCultureIgnoreCase))
                {
                    result.Body = field.Children[new YamlScalarNode("Value")].ToString();
                }
                else if (hint.Equals("ImageByline", StringComparison.InvariantCultureIgnoreCase))
                {
                    result.ImageByline = field.Children[new YamlScalarNode("Value")].ToString();
                }
                else if (hint.Equals("Image", StringComparison.InvariantCultureIgnoreCase))
                {
                    result.Image = field.Children[new YamlScalarNode("Value")].ToString();
                }
                //else if (hint.Equals("InternalFileLink", StringComparison.InvariantCultureIgnoreCase))
                //{
                //    
                //}
                //else if (hint.Equals("Media", StringComparison.InvariantCultureIgnoreCase))
                //{
                //    var mediaId = field.Children[new YamlScalarNode("Value")].ToString();
                //    if (!string.IsNullOrEmpty(mediaId))
                //    {
                //        result.Media = result.MediaMap.Where(m => m.Key == mediaId).FirstOrDefault().Value;
                //    }
                        
                //}
            }

            if (!string.IsNullOrEmpty(result.Image))
            {
                result.Image = result.Title;
            }

            return result;
        }
    }
}
