﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using YamlDotNet.RepresentationModel;

namespace Trasportministeriet.Migration
{
    public class ContentImporter
    {
        private IContentService _contentService = ApplicationContext.Current.Services.ContentService;
        private MediaImporter _mediaImport = new MediaImporter();
        private IMedia NyhederMediaRootFolderitem;
        private IMedia PolitiskeAftalerMediaRootFolderitem;
        private IMedia PublikationerMediaRootFolderitem;
        private IMedia TalerOgArtiklerMediaRootFolderitem;
        private IMedia NyhederImageRootFolderitem;
        private IMedia PolitiskeAftalerImageRootFolderitem;
        private IMedia PublikationerImageRootFolderitem;
        private IMedia TalerOgArtiklerImageRootFolderitem;
        private IMedia NyhederFileRootFolderitem;
        private IMedia PolitiskeAftalerFileRootFolderitem;
        private IMedia PublikationerFileRootFolderitem;
        private IMedia TalerOgArtiklerFileRootFolderitem;

        public ContentImporter()
        {
            NyhederMediaRootFolderitem = _mediaImport.GetParentFolder("Nyheder");
            PolitiskeAftalerMediaRootFolderitem = _mediaImport.GetParentFolder("Politiske aftaler");
            PublikationerMediaRootFolderitem = _mediaImport.GetParentFolder("Publikationer");
            TalerOgArtiklerMediaRootFolderitem = _mediaImport.GetParentFolder("Taler og artikler");

            NyhederImageRootFolderitem = _mediaImport.GetSubFolder(NyhederMediaRootFolderitem.Id, "Images");
            PolitiskeAftalerImageRootFolderitem = _mediaImport.GetSubFolder(PolitiskeAftalerMediaRootFolderitem.Id, "Images");
            PublikationerImageRootFolderitem = _mediaImport.GetSubFolder(PublikationerMediaRootFolderitem.Id, "Images");
            TalerOgArtiklerImageRootFolderitem = _mediaImport.GetSubFolder(TalerOgArtiklerMediaRootFolderitem.Id, "Images");

            NyhederFileRootFolderitem = _mediaImport.GetSubFolder(NyhederMediaRootFolderitem.Id, "Files");
            PolitiskeAftalerFileRootFolderitem = _mediaImport.GetSubFolder(PolitiskeAftalerMediaRootFolderitem.Id, "Files");
            PublikationerFileRootFolderitem = _mediaImport.GetSubFolder(PublikationerMediaRootFolderitem.Id, "Files");
            TalerOgArtiklerFileRootFolderitem = _mediaImport.GetSubFolder(TalerOgArtiklerMediaRootFolderitem.Id, "Files");
        }

        public IContent FindSubFolder(string name)
        {
            var homePage = _contentService.GetRootContent().Where(c => c.ContentType.Alias == "website").FirstOrDefault();
            if (homePage == null)
            {
                return null;
            }

            if (name.Equals("Nyheder", StringComparison.InvariantCultureIgnoreCase))
            {
                var folderItem = homePage.Children().Where(c => c.Name.Equals("Nyheder", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (folderItem == null)
                {
                    folderItem = _contentService.CreateContent("Nyheder", homePage.Id, "newsPage");
                    _contentService.SaveAndPublishWithStatus(folderItem);
                }

                return folderItem;
            }

            if (name.Equals("Politiske aftaler", StringComparison.InvariantCultureIgnoreCase))
            {
                var folderItem = homePage.Children().Where(c => c.Name.Equals("Politiske aftaler", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (folderItem == null)
                {
                    folderItem = _contentService.CreateContent("Politiske aftaler", homePage.Id, "newsPage");
                    _contentService.SaveAndPublishWithStatus(folderItem);
                }

                return folderItem;
            }

            if (name.Equals("Publikationer", StringComparison.InvariantCultureIgnoreCase))
            {
                var folderItem = homePage.Children().Where(c => c.Name.Equals("Publikationer Eng", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (folderItem == null)
                {
                    folderItem = _contentService.CreateContent("Publikationer Eng", homePage.Id, "newsPage");
                    _contentService.SaveAndPublishWithStatus(folderItem);
                }

                return folderItem;
            }

            if (name.Equals("Taler og artikler", StringComparison.InvariantCultureIgnoreCase))
            {
                var ministerietFolder = homePage.Children().Where(c => c.Name.Equals("Ministeren", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (ministerietFolder == null)
                {
                    ministerietFolder = _contentService.CreateContent("Ministeren", homePage.Id, "subPage");
                    _contentService.SaveAndPublishWithStatus(ministerietFolder);
                }

                var folderItem = ministerietFolder.Children().Where(c => c.Name.Equals("Taler og artikler", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (folderItem == null)
                {
                    folderItem = _contentService.CreateContent("Taler og artikler", ministerietFolder.Id, "newsPage");
                    _contentService.SaveAndPublishWithStatus(folderItem);
                }

                return folderItem;
            }
            return null;
        }

        public void CreateContent(Dictionary<string, List<SitecoreContent>> contents, IContent parentFolder, string contentType = "")
        {
            foreach (var year in contents.Keys)
            {
                var folderItem = parentFolder.Children().Where(c => c.Name.Equals(year, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (folderItem == null)
                {
                    folderItem = _contentService.CreateContent(year, parentFolder.Id, "library");
                    _contentService.Save(folderItem);
                }

                var yearContents = contents[year];

                foreach (var yearContent in yearContents)
                {
                    var publishDate = DateTime.Now;
                    var contentItem = folderItem.Children().Where(c => c.Name.Equals(yearContent.Title, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

                    if (contentItem == null)
                    {
                        contentItem = _contentService.CreateContent(yearContent.Title, folderItem.Id, "articlePage");
                    }

                    contentItem.ReleaseDate = publishDate;
                    contentItem.CreateDate = publishDate;
                    contentItem.UpdateDate = publishDate;

                    //Import nyheder content
                    if (contentType == "news")
                    {

                        contentItem.SetValue("title", yearContent.Title);
                        contentItem.SetValue("summary", yearContent.Summary);
                        contentItem.SetValue("date", yearContent.Date);
                        contentItem.SetValue("imageByline", yearContent.ImageByline);
                        contentItem.SetValue("body", yearContent.Body);

                        if (yearContent.InternalFileLinks != null)
                        {

                            if (yearContent.InternalFileLinks.Any())
                            {
                                var yearFilesFolderItem = _mediaImport.GetSubFolder(NyhederFileRootFolderitem.Id, year);
                                //When we export the files, we create a folder using the page title, and put all the files used in the page into this folder.
                                var subFolderItem = yearFilesFolderItem.Children().Where(c => c.Name.Contains(yearContent.Title)).FirstOrDefault();
                                if (subFolderItem != null)
                                {
                                    var files = subFolderItem.Children().Select(c => c.Id);
                                    contentItem.SetValue("internalFileLink", string.Join(",", files));
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(yearContent.Image))
                        {
                            var yearImageFolderItem = _mediaImport.GetSubFolder(NyhederImageRootFolderitem.Id, year);
                            //When export images, we use the page title as the image name so we can locate the image by using the page title.
                            var imgItem = yearImageFolderItem.Children().Where(c => c.Name.Contains(yearContent.Title)).FirstOrDefault();
                            if (imgItem != null)
                            {
                                contentItem.SetValue("Image", imgItem.Id);
                            }

                        }
                    }
                    //Import the other content
                    else
                    {
                        contentItem.SetValue("title", yearContent.Title);
                        contentItem.SetValue("summary", yearContent.Summary);
                        contentItem.SetValue("date", yearContent.Date);
                        contentItem.SetValue("imageByline", yearContent.ImageByline);
                        contentItem.SetValue("body", yearContent.Body);
                        if (yearContent.InternalFileLinks != null)
                        {
                            if (yearContent.InternalFileLinks.Any())
                            {
                                var yearFilesFolderItem = _mediaImport.GetSubFolder(NyhederFileRootFolderitem.Id, year);
                                //When we export the files, we create a folder using the page title, and put all the files used in the page into this folder.
                                var subFolderItem = yearFilesFolderItem.Children().Where(c => c.Name.Contains(yearContent.Title)).FirstOrDefault();
                                if (subFolderItem != null)
                                {
                                    var files = subFolderItem.Children().Select(c => c.Id);
                                    contentItem.SetValue("internalFiles", string.Join(",", files));
                                }
                            }
                        }
                    }
                    _contentService.Save(contentItem);
                }
                //var contentItem = _contentService.CreateContent(content.Title, parentFolderId, "NewsModules");

                //_contentService.Save(contentItem);
            }
        }
    }
}
