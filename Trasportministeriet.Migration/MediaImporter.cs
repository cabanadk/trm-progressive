﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace Trasportministeriet.Migration
{
    public class MediaImporter
    {
        IMediaService _mediaService;

        public MediaImporter()
        {
            _mediaService = ApplicationContext.Current.Services.MediaService;
        }

        public void Import(string imagePath, string imageName, Guid folderId, string mediaType)
        {
            //var media = _mediaService.CreateMedia(imageName, folderId, mediaType);
            //FileStream file = new FileStream(imagePath, File.Open);
            //media.SetValue(imageName, Path.GetFileName(imagePath), file);
            //_mediaService.Save(media);
            //file.Close();
        }

        public IMedia CreateMediaFolder(string folderName, Guid parentFolder)
        {
            var folder = parentFolder == Guid.Empty ? _mediaService.CreateMedia(folderName, -1, "Folder") : _mediaService.CreateMedia(folderName, parentFolder, "Folder");
            _mediaService.Save(folder);
            return folder;
        }

        public IMedia CreateMediaFolder(string folderName, int parentFolder)
        {
            var folder = _mediaService.CreateMedia(folderName, parentFolder, "Folder");
            _mediaService.Save(folder);
            return folder;
        }
        public IMedia GetParentFolder(string folderName)
        {
            var folder = _mediaService.GetRootMedia()
                                 .Where(m => m.ContentType.Name == "Folder"
                                              && m.Name.Equals(folderName)).FirstOrDefault();

            return folder;
        }

        public IMedia GetSubFolder(int parentFolderId, string folderName)
        {
            var folder = _mediaService.GetChildren(parentFolderId)
                                .Where(c => c.ContentType.Name == "Folder"
                                             && c.Name.Equals(folderName)).FirstOrDefault();

            return folder;
        }

        public List<IMedia> CreateMediaFolders(IMedia parentFolder, string folderType, string uploadMediaRootFolder)
        {
            var folderItems = new List<IMedia>();
            var mediaTypeFolder = GetSubFolder(parentFolder.Id, folderType);
            if (mediaTypeFolder == null)
            {
                mediaTypeFolder = CreateMediaFolder(folderType, parentFolder.Id);
            }

            var subFolders = Directory.GetDirectories(uploadMediaRootFolder);
            foreach (var folder in subFolders)
            {
                var folderName = Path.GetFileName(folder);
                var folderItem = GetSubFolder(mediaTypeFolder.Id, folderName);
                if (folderItem == null)
                {
                    folderItem = CreateMediaFolder(folderName, mediaTypeFolder.Id);
                }
                folderItems.Add(folderItem);
            }

            return folderItems;
        }

        public int UploadMedia(string imageSrc, IMedia parentFolder, string mediaType="Image")
        {
            IMedia newImage = null;
            try
            {
                var imageName = Path.GetFileName(imageSrc);

                var media = parentFolder.Children().Where(c => c.Name.Contains(imageName)).FirstOrDefault();

                if (media != null)
                {
                    return media.Id;
                }

                // Here we are creating the folders and adding images to them
                newImage = _mediaService.CreateMedia(imageName, parentFolder.Id, mediaType);

                var buffer = System.IO.File.ReadAllBytes(imageSrc);

                newImage.SetValue("umbracoFile", imageName, new MemoryStream(buffer));
                _mediaService.Save(newImage);
            }
            catch (Exception ex)
            {
                // log the exception
            }
            return newImage.Id;
        }

    }
}
