$(function() {
    if ( $( ".search-field" ).length < 0 ) {
        return;
    }
    // Slides up/down the serach field
    $(document).on("click",".search-js",function() {
        var searchField = $(".search-field-js");

        searchField.slideToggle(200).css("top", "100px")
        
    });
});