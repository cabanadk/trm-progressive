( function() {
    if ( $( ".video" ).length < 0 ) {
        return;
    }
    $(document).on("click",".video",function(e) {
        e.preventDefault();
        var id = $(this).data('embed');
        var height = $(this).find("img").height()
        var iframe = document.createElement( "iframe" );
        iframe.setAttribute( "id", "video" );
        iframe.setAttribute( "frameborder", "0" );
        iframe.setAttribute( "allowfullscreen", "" );
        iframe.setAttribute( "src", "https://dreambroker.com/channel/8obqba5n/iframe/"+ id + "?autoplay=1");
        this.appendChild( iframe );
    });
} )();