$(function() {
  if ( $( ".filter__years" ).length < 0 ) {
      return;
  }
  // Slides up/down the serach field
  $(".filter__years select").change(function(){
      var selected = $(this).children("option:selected");
      if(selected.prop('index') === 0){
          $('.overview .overview__item').fadeIn(600);
      } else {
          var filterValue = selected.val().toLowerCase().split(' ').join('').split('&').join('-');
          $('.overview .overview__item').not('.' + filterValue).hide(200);
          $('.overview .overview__item'+ filterValue).fadeIn(600);
      }
  });
});