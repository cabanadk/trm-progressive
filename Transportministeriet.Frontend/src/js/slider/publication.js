$(function() {
  "use strict";
  if ($(".publications-slider").length < 0) {
    return;
  }
  var owl = $(".publications-slider").owlCarousel({
    lazyLoad: true,
    stagePadding: 70,
    margin: 10,
    nav: true,
    navText: [$('.prev'),$('.next')],
    responsive: {
      0: {
        items: 1,
        margin: 100,
        stagePadding: 100,
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  });


});
