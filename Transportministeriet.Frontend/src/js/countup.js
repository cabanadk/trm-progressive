$(function() {
  if ($(".info-box__counter").length < 0) {
    return;
  }
  // amkes sure it only runnes once.
  var runned = false;

  // First run the count when in viewport
  $(".info-box__counter").isInViewport(function(status) {
    if (status === "entered") {
      countUp();
    }
  });
  // Count up numbers to the data-count
  function countUp() {
    if (!runned) {
      $(".counter").each(function() {
        var $this = $(this),
          countTo = $this.attr("data-count");

        $({ countNum: $this.text() }).animate(
          {
            countNum: countTo
          },

          {
            duration: 1000,
            easing: "linear",
            step: function() {
              $this.text(Math.floor(this.countNum));
            },
            complete: function() {
              $this.text(this.countNum);
              //alert('finished');
            }
          }
        );
        runned = true;
      });
    }
  }
});
