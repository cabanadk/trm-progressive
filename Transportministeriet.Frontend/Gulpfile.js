/// <binding Clean='clean' ProjectOpened='watch' />
"use strict";
var production = false;

var paths = {
    src: "src/**/*",
    srcFonts: 'src/fonts/*.{eot,svg,ttf,woff,woff2}',
    srcLayouts: "src/layouts/*.html",
    srcSass: "src/sass/app.scss",
    srcJs: "src/js/app.js",
    srcImages: "src/images/**/*",
    deployLayout: "deploy/",
    deployCss: "deploy/assets/css",
    deployJs: "deploy/assets/js",
    deployImages: "deploy/assets/images",
    deployFonts: "deploy/assets/fonts",
    umbracoCss: "../Trasportministeriet.Web/assets/css",
    umbracoJs: "../Trasportministeriet.Web/assets/js",
    umbracoImages: "../Trasportministeriet.Web/assets/images",
    umbracoFonts: "../Trasportministeriet.Web/assets/fonts"
};

/*********************************************
 *   Required
 *********************************************/
var gulp = require("gulp"),
  babel = require('gulp-babel'),
  autoprefixer = require("gulp-autoprefixer"),
  include = require("gulp-include"),
  rimraf = require("gulp-rimraf"),
  sass = require("gulp-sass"),
  sourcemaps = require("gulp-sourcemaps"),
  beautify = require("gulp-beautify"),
  terser = require('gulp-terser'),
  cleanCSS = require("gulp-clean-css"),
  gulpif = require("gulp-if"),
  imagemin = require("gulp-imagemin"),
  rename = require("gulp-rename"),
  fs = require("fs"),
  sequence = require("run-sequence"),
  browserSync = require('browser-sync').create(),
  browserify = require('gulp-browserify');
  

function swallowError(error) {
  // If you want details of the error in the console
    this.emit("end");
    return console.log(error.toString());
}



/******************************************
 *   CLEAN
 *******************************************/
gulp.task("clean", function() {
  return gulp
    .src("./deploy", { read: false }) // much faster
    .pipe(rimraf({ force: true }));
});

/******************************************
 *   CACHE BUSTING
 *******************************************/

var uuid = "xxxxxxxx-xxxx-Txxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
  var d = new Date().getTime();
  var r = (d + Math.random() * 16) % 16 | 0;
  d = Math.floor(d / 16);
  return (c === "x" ? r : (r & 0x7) | 0x8).toString(16);
});

gulp.task("fingerprint", function(cb) {
  fs.writeFile("./deploy/assets/fingerprint.json", '{"id":"' + uuid + '"}', cb);
});

/*========================================
  SASS
========================================*/

gulp.task("sass", function() {
  gulp
    .src(paths.srcSass)
    .pipe(include())
    .pipe(sourcemaps.init({ loadMaps: true })) // Process the original sources
    .pipe(sass())
    .on("error", swallowError)
    .pipe(
      autoprefixer({
        browsers: ["last 4 versions"],
        cascade: false
      })
    )

    .pipe(sourcemaps.write()) // Add the map to modified source.
    .pipe(rename("app.css"))
    .pipe(gulpif(production, cleanCSS()))
    .pipe(gulp.dest(paths.deployCss))
    .pipe(gulp.dest(paths.umbracoCss))
    .pipe(browserSync.stream());
});

/*========================================
  JS
========================================*/




gulp.task("js", function() {
  gulp
    .src(paths.srcJs)
    .pipe(include())
    .pipe(browserify({
      insertGlobals : true,
      debug : !gulp.env.production
    })).pipe(sourcemaps.init())
    .pipe(gulpif(production, terser()))
    .pipe(sourcemaps.write())
    .on("error", swallowError)
    .pipe(gulpif(production, rename({ suffix: ".min" })))
    .pipe(gulp.dest(paths.deployJs))
    .pipe(gulp.dest(paths.umbracoJs))
    .pipe(gulpif(!production, browserSync.stream()))
});

/*========================================
  HTML
========================================*/
gulp.task("html", function() {
  gulp
    .src(paths.srcLayouts)
    .pipe(include())
    .pipe(gulp.dest(paths.deployLayout))
    .pipe(browserSync.stream());
});

/*========================================
  Images
========================================*/
gulp.task("images", function() {
  gulp
    .src(paths.srcImages)
    .pipe(
      gulpif(
        production,
        imagemin([
          imagemin.gifsicle({ interlaced: true }),
          imagemin.jpegtran({ progressive: true }),
          imagemin.optipng({ optimizationLevel: 5 }),
          imagemin.svgo({
            plugins: [{ removeViewBox: true }, { cleanupIDs: false }]
          })
        ])
      )
    )
    .pipe(gulp.dest(paths.deployImages))
    .pipe(gulp.dest(paths.umbracoImages));
});

/******************************************
 *   FONTS
 *******************************************/
gulp.task("fonts", function() {
    gulp
        .src(paths.srcFonts)
        .pipe(gulp.dest(paths.deployFonts))
        .pipe(gulp.dest(paths.umbracoFonts))
});

/*========================================
  WATCH
========================================*/

gulp.task("watch", ["build"], function() {

   browserSync.init({
        server: {
            baseDir: "./deploy/",
            index: "index.html"
        }
    });

  gulp.watch("src/sass/**/*.scss", ["sass"]).on('change', browserSync.reload);
  gulp.watch("src/js/**/*.js", ["js"]).on('change', browserSync.reload);
  gulp.watch("src/layouts/**/*.html", ["html"]).on('change', browserSync.reload);
  gulp.watch("src/images/**/*", ["images"]).on('change', browserSync.reload);
  gulp.watch("src/fonts/**/*", ["fonts"]);
  
});

gulp.task("build", function() {
  sequence(["sass", "js", "html", "images", "fonts"]);
});

gulp.task("deploy", function() {
  production = true;
  sequence("clean", ["build"]);
});
