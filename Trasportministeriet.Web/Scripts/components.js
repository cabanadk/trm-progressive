﻿//News
$('select').on('change', function () {
    var year = $(this).find('option:selected').data("year");
    var id = $(this).find('option:selected').attr('id');
    var ItemsShown = $('.starting-items').data("startingitems");
        $('.news').empty();
        $('#loadmore').removeClass('d-none')
        $.ajax({
            type: 'POST',
            url: '/umbraco/api/SortNews/Sort?year=' + year + "&id=" + id + "&ItemsToDisplay=" + ItemsShown,
            success: function (data) {
                console.log(data);
                if (data[0].HideButton) {
                    $('#loadmore').addClass('d-none');
                }
                data.forEach((el) => {
                    $('.news').append(`
                        <a href="${el.url}" class="col-12">
                            <div class="row item">
                                <div class="col-12 col-md-2">
                                    <time datatime="${el.date}">${el.date}</time>
                                </div>
                  
                                <div class="col text">
                                    <h2>${el.header}</h2>
                                    <p>${el.desc}</p>
                                </div>

                                <div class="col-12 col-md-3">
                                <img src="${el.img}"> 
                                </div>
                             </div>
                        </a>`
                    );
                });
            }
        });

});
//load more
$('#loadmore').click(function () {
    var ItemsOnScreen = $(".news > a").length;
    var year = $('select').find('option:selected').data("year");
    var id = $('select').find('option:selected').attr('id');
    var ItemsShown = $('.starting-items').data("startingitems");
   
    var ItemsToDisplay = ItemsOnScreen + ItemsShown
    $('.news').empty();

    $.ajax({
        type: 'POST',
        url: '/umbraco/api/SortNews/Sort?year=' + year + "&id=" + id + "&ItemsToDisplay=" + ItemsToDisplay,

        success: function (data) {
            console.log(data[0].HideButton);
            if (data[0].HideButton) {
                $('#loadmore').addClass('d-none');
            }
            data.forEach((el) => {

                $('.news').append(`<a href="${el.url}" class="col-12">
                        <div class="row item">

                                <div class="col-12 col-md-2">
                                    <time datatime="${el.date}">${el.date}</time>
                                </div>
                  
                                <div class="col text">
                                    <h2>${el.header}</h2>
                                    <p>${el.desc}</p>
                                </div>

                                <div class="col-12 col-md-3">
                                <img src="${el.img}"> 
                                </div>
                            </div>
                    </a>`
                );
            });
        }

    })

})


//Subscribe To newsletter
$('#Subscribe').click(function () {
    var email = $('#email').val()
    $.ajax({
        type: 'POST',
        url: '/umbraco/api/SubscribeToNewsletter/Subscribe?email=' + "test",

        success: function (data) {
            console.log(data, email)
        }
    })
});

//Articles-Publications
$('.articles').children().click(function () {
    var year = $(this).data("year");
    var id = this.id
    var count = $(".owl-stage > div").length;
    console.log(count);
    for (var i = 0; i < count; i++) {
        $(".publications-slider").trigger('remove.owl.carousel', [i])
            .trigger('refresh.owl.carousel');
    }

    console.log(year, id)

    var owl = $(".publications-slider").owlCarousel({
        lazyLoad: true,
        stagePadding: 70,
        margin: 10,
        nav: false,
        responsive: {
            0: {
                items: 1,
                margin: 30,
                stagePadding: 60,
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });


    $.ajax({
        type: 'POST',
        url: '/umbraco/api/SortPublications/Sort?year=' + year + "&id= " + id,

        success: function (data) {
            console.log(data)
            data.forEach((el) => {
                var html = `<a href="${el.Url}">
                                                    <div  class="publication">
                                                        <img src="${el.Img}" alt=""/>
                                                        <small class="date_tag"> ${el.Date}<small/>
                                                        <p class="title">
                                                            ${el.Title}
                                                        </p>
                                                    </div>
                                                  </a>
                                                `;

                owl.trigger('add.owl.carousel', ['<div class="owl-item">' + html + '</div>'])
                    .trigger('refresh.owl.carousel');

               

            })

        }
    })
})
