[
    {
        "name": "Rich text editor",
        "alias": "rte",
        "view": "rte",
        "icon": "icon-article"
    },
    {
        "name": "Image",
        "alias": "media",
        "view": "media",
        "icon": "icon-picture"
    },
    {
        "name": "Macro",
        "alias": "macro",
        "view": "macro",
        "icon": "icon-settings-alt"
    },
    {
        "name": "Embed",
        "alias": "embed",
        "view": "embed",
        "icon": "icon-movie-alt"
    },
    {
        "name": "Headline",
        "alias": "headline",
        "view": "textstring",
        "icon": "icon-coin",
        "config": {
            "style": "font-size: 36px; line-height: 45px; font-weight: bold",
            "markup": "<h1>#value#</h1>"
        }
    },
    {
        "name": "Quote",
        "alias": "quote",
        "view": "textstring",
        "icon": "icon-quote",
        "config": {
            "style": "border-left: 3px solid #ccc; padding: 10px; color: #ccc; font-family: serif; font-style: italic; font-size: 18px",
            "markup": "<blockquote>#value#</blockquote>"
        }
    },
    {
        "name": "Hero",
        "alias": "Hero",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-forms-github",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "HeroContent",
                    "alias": "heroContent",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                },
                {
                    "name": "test",
                    "alias": "test",
                    "propretyType": {},
                    "dataType": "92897bc6-a5f3-4ffe-ae27-f2e7e33dda49"
                }
            ],
            "frontView": "",
            "renderInGrid": "0"
        }
    },
    {
        "name": "Statistics",
        "alias": "statistics",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-forms-table",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Statistics",
                    "alias": "statistics",
                    "propretyType": {},
                    "dataType": "38913585-21eb-4ec2-a703-2fe36ac587a1"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "News",
        "alias": "news",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-newspaper-alt",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "NewsContentPicker",
                    "alias": "newsContentPicker",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59",
                    "description": "Select a page here or fill out the data by you self under here."
                },
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Date",
                    "alias": "date",
                    "propretyType": {},
                    "dataType": "5046194e-4237-453c-a547-15db3a07c4e1"
                },
                {
                    "name": "Image",
                    "alias": "image",
                    "propretyType": {},
                    "dataType": "de915099-418b-43c1-b3e4-0b6a245dcb52"
                },
                {
                    "name": "Summary",
                    "alias": "summary",
                    "propretyType": {},
                    "dataType": "ca90c950-0aff-4e72-b976-a30b1ac57dad"
                },
                {
                    "name": "Link",
                    "alias": "link",
                    "propretyType": {},
                    "dataType": "451add15-99b2-44d0-a97c-fff43ac2a7d8"
                }
            ],
            "frontView": "",
            "renderInGrid": "0"
        }
    },
    {
        "name": "Title with Link",
        "alias": "titleWithLink",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-untitled",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Links",
                    "alias": "links",
                    "propretyType": {},
                    "dataType": "451add15-99b2-44d0-a97c-fff43ac2a7d8"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "Link box",
        "alias": "linkBox",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-link",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "links",
                    "alias": "links",
                    "propretyType": {},
                    "dataType": "b4e3535a-1753-47e2-8568-602cf8cfee6f"
                },
                {
                    "name": "BackgroundColor",
                    "alias": "backgroundColor",
                    "propretyType": {},
                    "dataType": "92897bc6-a5f3-4ffe-ae27-f2e7e33dda49",
                    "description": "sæt til true for at gøre baggrund hvid"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "Article Component",
        "alias": "articleComponent",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-invoice color-green",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Header",
                    "alias": "header",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Link",
                    "alias": "link",
                    "propretyType": {},
                    "dataType": "451add15-99b2-44d0-a97c-fff43ac2a7d8"
                },
                {
                    "name": "Article Picker",
                    "alias": "articlePicker",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                },
                {
                    "name": "Items To show",
                    "alias": "itemsToShow",
                    "propretyType": {},
                    "dataType": "2e6d3631-066e-44b8-aec4-96f09099b2b5"
                }
            ],
            "frontView": "",
            "renderInGrid": "0"
        }
    },
    {
        "name": "Business Card",
        "alias": "businessCard",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-multiple-credit-cards",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "business Card Picker",
                    "alias": "businessCardPicker",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "Sorted News",
        "alias": "sortedNews",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-newspaper",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Header",
                    "alias": "header",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Items Shown",
                    "alias": "itemsShown",
                    "propretyType": {},
                    "dataType": "2e6d3631-066e-44b8-aec4-96f09099b2b5"
                },
                {
                    "name": "News Picker",
                    "alias": "newsPicker",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                }
            ],
            "frontView": "",
            "renderInGrid": "0"
        }
    },
    {
        "name": "Subscribe To Newsletter",
        "alias": "subscribeToNewsletter",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-message",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Component Picker",
                    "alias": "componentPicker",
                    "propretyType": {},
                    "dataType": "b1c0ea69-da54-4b83-93dd-7e57435c1f2c"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "Image Picker",
        "alias": "imagePicker",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-pictures-alt-2",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Image Picker",
                    "alias": "imagePicker",
                    "propretyType": {},
                    "dataType": "135d60e0-64d9-49ed-ab08-893c9ba44ae5"
                }
            ],
            "frontView": "",
            "renderInGrid": "0"
        }
    },
    {
        "name": "Sorted Articles",
        "alias": "sortedArticles",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-ordered-list",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Name",
                    "alias": "name",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Link",
                    "alias": "link",
                    "propretyType": {},
                    "dataType": "451add15-99b2-44d0-a97c-fff43ac2a7d8"
                },
                {
                    "name": "Articles Picker",
                    "alias": "articlesPicker",
                    "propretyType": {},
                    "dataType": "336a3327-2992-4b20-9b78-ce198782ff53"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "Social media",
        "alias": "socialMedia",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-molecular-network",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Number",
                    "alias": "number",
                    "propretyType": {},
                    "dataType": "dda75bb3-161e-4e7a-aec5-f30b12d8f913",
                    "description": "Number of Social media showed"
                },
                {
                    "name": "UserID",
                    "alias": "userID",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae",
                    "description": "Username of the profile where the images should shown from"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "Title Social Media",
        "alias": "titleSocialMedia",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-iphone",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "renderInGrid": "0",
            "frontView": "",
            "editors": [
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "SubTitle",
                    "alias": "subTitle",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Social Media",
                    "alias": "socialMedia",
                    "propretyType": {},
                    "dataType": "f0908745-3cb8-4a51-95ce-dedac141c957"
                }
            ]
        }
    },
    {
        "name": "Tema Banner",
        "alias": "temaBanner",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-flag-alt",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "TemahighLight",
                    "alias": "temahighLight",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "MorePublications",
        "alias": "morePublications",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-forms-stackoverflow color-green",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "PublicationsPicker",
                    "alias": "publicationsPicker",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                },
                {
                    "name": "Max Publications",
                    "alias": "maxPublications",
                    "propretyType": {},
                    "dataType": "2e6d3631-066e-44b8-aec4-96f09099b2b5",
                    "description": "Vælg hvor mange publication der skal vises"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "Title and Lead Box",
        "alias": "titleAndLeadBox",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-thumbnail-list",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Lead",
                    "alias": "lead",
                    "propretyType": {},
                    "dataType": "c259bac0-faab-4615-b220-5bcfa7bab558"
                },
                {
                    "name": "Date",
                    "alias": "date",
                    "propretyType": {},
                    "dataType": "e4d66c0f-b935-4200-81f0-025f7256b89a"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "Themes",
        "alias": "themes",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-thumbnails-small",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Theme Picker",
                    "alias": "themePicker",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "ThemesLarge",
        "alias": "themesLarge",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-thumbnails",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Theme Picker",
                    "alias": "themePicker",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "QuoteTransport",
        "alias": "quoteTransport",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-quote",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Quote",
                    "alias": "quote",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Author",
                    "alias": "author",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "Video Picker",
        "alias": "videoPicker",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-next",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Start image",
                    "alias": "startImage",
                    "propretyType": {},
                    "dataType": "135d60e0-64d9-49ed-ab08-893c9ba44ae5"
                },
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "VideoId",
                    "alias": "videoId",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Body",
                    "alias": "body",
                    "propretyType": {},
                    "dataType": "c6bac0dd-4ab9-45b1-8e30-e4b619ee5da3"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "LinksWithDownload",
        "alias": "linksWithDownload",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-settings-alt",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "renderInGrid": "0",
            "frontView": "",
            "editors": [
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Media1",
                    "alias": "media1",
                    "propretyType": {},
                    "dataType": "9dbbcbbb-2327-434a-b355-af1b84e5010a"
                },
                {
                    "name": "Media2",
                    "alias": "media2",
                    "propretyType": {},
                    "dataType": "9dbbcbbb-2327-434a-b355-af1b84e5010a"
                },
                {
                    "name": "Media3",
                    "alias": "media3",
                    "propretyType": {},
                    "dataType": "9dbbcbbb-2327-434a-b355-af1b84e5010a"
                },
                {
                    "name": "Media4",
                    "alias": "media4",
                    "propretyType": {},
                    "dataType": "9dbbcbbb-2327-434a-b355-af1b84e5010a"
                },
                {
                    "name": "Media5",
                    "alias": "media5",
                    "propretyType": {},
                    "dataType": "9dbbcbbb-2327-434a-b355-af1b84e5010a"
                },
                {
                    "name": "Media6",
                    "alias": "media6",
                    "propretyType": {},
                    "dataType": "9dbbcbbb-2327-434a-b355-af1b84e5010a"
                }
            ]
        }
    },
    {
        "name": "Hero Business Card",
        "alias": "heroBusinessCard",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-forms-github",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "img Picker",
                    "alias": "imgPicker",
                    "propretyType": {},
                    "dataType": "135d60e0-64d9-49ed-ab08-893c9ba44ae5"
                },
                {
                    "name": "Business Card Picker",
                    "alias": "businessCardPicker",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                }
            ],
            "frontView": "",
            "renderInGrid": "0"
        }
    },
    {
        "name": "Spot box",
        "alias": "spotBox",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-shipping-box",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "BodyText",
                    "alias": "bodyText",
                    "propretyType": {},
                    "dataType": "ca90c950-0aff-4e72-b976-a30b1ac57dad"
                },
                {
                    "name": "Links",
                    "alias": "links",
                    "propretyType": {},
                    "dataType": "451add15-99b2-44d0-a97c-fff43ac2a7d8"
                },
                {
                    "name": "Media Picker",
                    "alias": "mediaPicker",
                    "propretyType": {},
                    "dataType": "de915099-418b-43c1-b3e4-0b6a245dcb52"
                }
            ],
            "frontView": ""
        }
    },
    {
        "name": "Latest News",
        "alias": "latestNews",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-forms-file-word",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "renderInGrid": "0",
            "frontView": "",
            "editors": [
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Links",
                    "alias": "links",
                    "propretyType": {},
                    "dataType": "451add15-99b2-44d0-a97c-fff43ac2a7d8"
                },
                {
                    "name": "NewsPicker",
                    "alias": "newsPicker",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                }
            ]
        }
    },
    {
        "name": "More Publications 4 col",
        "alias": "morePublications4Col",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-notepad-alt",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Publications Picker",
                    "alias": "publicationsPicker",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59",
                    "description": ""
                },
                {
                    "name": "Max Publications",
                    "alias": "maxPublications",
                    "propretyType": {},
                    "dataType": "2e6d3631-066e-44b8-aec4-96f09099b2b5"
                },
                {
                    "name": "Links",
                    "alias": "links",
                    "propretyType": {},
                    "dataType": "451add15-99b2-44d0-a97c-fff43ac2a7d8"
                }
            ],
            "frontView": "",
            "renderInGrid": "0"
        }
    },
    {
        "name": "Downloads",
        "alias": "downloads",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-page-down",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Header",
                    "alias": "header",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Download Links",
                    "alias": "downloadLinks",
                    "propretyType": {},
                    "dataType": "247733a6-8416-42d9-9345-88a550e43aba"
                }
            ],
            "frontView": "",
            "renderInGrid": "0"
        }
    },
    {
        "name": "StatisticsWithImgs",
        "alias": "statisticsWithImgs",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-forms-table",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Statistics",
                    "alias": "statistics",
                    "propretyType": {},
                    "dataType": "38913585-21eb-4ec2-a703-2fe36ac587a1"
                }
            ],
            "frontView": ""
        }
    },
    {
        "name": "Creative commons licens",
        "alias": "creativeCommonsLicens",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-diploma-alt",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "expiration": 6400,
            "frontView": ""
        }
    }
]