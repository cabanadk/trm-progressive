﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trasportministeriet.Migration;

namespace Trasportministeriet.Web.Umbraco
{
    public partial class ImportContent : System.Web.UI.Page
    {
        private ContentImporter _contentImporter = new ContentImporter();
        private YamlParser _yamlParser = new YamlParser();
        private string _yamlSourceFolder;

        protected void Page_Load(object sender, EventArgs e)
        {
            _yamlSourceFolder = Server.MapPath("~/App_Data/Migration/Content");
        }

        protected void ImportNews_Click(object sender, EventArgs e)
        {
            var folderItem = _contentImporter.FindSubFolder("Nyheder");
            if (folderItem == null)
            {
                return;
            }

            var newsContents = _yamlParser.GetContents(_yamlSourceFolder + @"/nyheder/Nyheder");
            _contentImporter.CreateContent(newsContents, folderItem, "news");
        }

        protected void ImportPublications_Click(object sender, EventArgs e)
        {
            var folderItem = _contentImporter.FindSubFolder("Publikationer");
            if (folderItem == null)
            {
                return;
            }

            var newsContents = _yamlParser.GetContents(_yamlSourceFolder + @"/publikationer/Publikationer");
            _contentImporter.CreateContent(newsContents, folderItem);
        }

        protected void ImportAftaler_Click(object sender, EventArgs e)
        {
            var folderItem = _contentImporter.FindSubFolder("Politiske aftaler");
            if (folderItem == null)
            {
                return;
            }

            var newsContents = _yamlParser.GetContents(_yamlSourceFolder + @"/politiskeaftaler/Politiske aftaler");
            _contentImporter.CreateContent(newsContents, folderItem);
        }

        protected void ImportTaler_Click(object sender, EventArgs e)
        {
            var folderItem = _contentImporter.FindSubFolder("Taler og artikler");
            if (folderItem == null)
            {
                return;
            }

            var newsContents = _yamlParser.GetContents(_yamlSourceFolder + @"/taler/Taler og artikler");
            _contentImporter.CreateContent(newsContents, folderItem);
        }
    }
}