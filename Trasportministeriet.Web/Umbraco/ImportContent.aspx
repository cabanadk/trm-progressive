﻿<%@ Page Language="C#" AutoEventWireup="true" Codefile="ImportContent.aspx.cs" Inherits="Trasportministeriet.Web.Umbraco.ImportContent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button runat="server" ID="ImportNews" Text="Nyheder" OnClick="ImportNews_Click" />
            <asp:Button runat="server" ID="ImportPublications" Text="Publikationer" OnClick="ImportPublications_Click" />
            <asp:Button runat="server" ID="ImportAftaler" Text="Politiske aftaler" OnClick="ImportAftaler_Click" />
            <asp:Button runat="server" ID="ImportTaler" Text="Taler og artikler" OnClick="ImportTaler_Click" />
        </div>
    </form>
</body>
</html>
