﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Media.EmbedProviders.Settings;
using Umbraco.Web.PublishedContentModels;
using Umbraco.Web.WebApi;

namespace Trasportministeriet.Core.Controllers.api
{
    public class SortNewsController : UmbracoApiController
    {

        [HttpPost]
        [Route("api/[controller]")]
        public object Sort(string year, int id, int ItemsToDisplay)
        {
            bool HideButton = false;
            var newsFolder = Umbraco.TypedContent(id);
            var news = newsFolder.Children();
            IEnumerable<IPublishedContent> allnews = null;

            //its root news folder - return all news, 
            //TODO there is no error handling if they select wrong folder


            //select all
            if(year =="all")
            {
                allnews = newsFolder.Descendants().Where(x => x.IsVisible() && x.DocumentTypeAlias == "articlePage").OrderBy(x => x.GetPropertyValue<DateTime>("date")).Reverse();
                news = allnews.Take(ItemsToDisplay);
            }
            else 
            {
                //select by year
                allnews = newsFolder.Descendants().Where(x => x.DocumentTypeAlias == "articlePage")
                           .Where(x=>x.GetPropertyValue<DateTime>("date").ToString("yyyy")
                           .Equals(year))
                           .OrderBy(x => x.GetPropertyValue<DateTime>("date")).Reverse();
                news = allnews.Take(ItemsToDisplay);
            }
           
           
            //raise the hide button flag
            foreach (var newsItem in news)
            {
                if (newsItem == allnews.Last())
                {
                    HideButton = true;
                }
            }

            var results = new List<IPublishedContent>();
            var response = new List<object>();
            var sortedNews = news.OrderBy(x => x.GetPropertyValue<DateTime>("date")).Reverse();



            foreach (var content in sortedNews.Take(ItemsToDisplay))
            {
                var article = new ArticlePage(content);
                var result = new Dictionary<string, object>();

                var date = article.Date.ToString("dd. MMMM yyyy");
                var header = article.Title;
                var desc = article.Summary.ToString();
                var imageUrl = "";
                if (article.Image!=null)
                {
                     imageUrl = article.Image.GetCropUrl("ImgPicker");
                }
               
                

                result.Add("date", date);
                result.Add("header", header);
                result.Add("desc", desc);
                result.Add("img", imageUrl);
                result.Add("url", article.Url);
                result.Add("HideButton", HideButton);

                response.Add(result);

            }

            return response;

        }
    }
}
