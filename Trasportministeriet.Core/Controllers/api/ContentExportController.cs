﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace Trasportministeriet.Core.Controllers.api
{
    public class ContentExportController : UmbracoApiController
    {
        private static UmbracoHelper _umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
        private static string _host;
        public static int Count;
        [System.Web.Http.HttpGet]
        public JsonResult<ExportResponse> ExportArticles(int contentId)
        {
            _host = Request.RequestUri.Host;
            Count = 0;
            var node = Umbraco.TypedContent(contentId);
            var content = GetNode(node);
            var response = new ExportResponse() { Count = Count, Items = content };
            return Json(response);
        }

        private ExportItem GetNode(IPublishedContent node)
        {
            Count++;
            switch (node.DocumentTypeAlias)
            {
                case "articlePage":
                    //TODO FIX THIS
                    var image = node.GetPropertyValue<Media>("image");
                    var imageUrl = "";
                    if (image != null)
                    {
                        imageUrl = "";
                    }
                    return new ArticleExportItem()
                    {
                        Id = node.Id,
                        DocumentAlias = node.DocumentTypeAlias,
                        Title = node.GetPropertyValue<string>("title"),
                        Summary = node.GetPropertyValue<string>("summary"),
                        OffentliggjortMedie = node.GetPropertyValue<string>("offentliggjortMedie"),
                        ImageId = node.GetPropertyValue<string>("image"),
                        ImageUrl = imageUrl,
                        Quote = node.GetPropertyValue<string>("quote"),
                        ImageByLine = node.GetPropertyValue<string>("imageByline"),
                        Date = node.GetPropertyValue<DateTime>("date"),
                        Body = node.GetPropertyValue<string>("body"),
                        VideoId = node.GetPropertyValue<string>("newsVideoId"),
                        Tags = node.GetPropertyValue<string[]>("tag"),
                        Components = GetComponents(node),
                        Children = node.Children.Select(x => GetNode(x)).ToList()
                    };
                default:
                    return new ExportItem()
                    {
                        Name = node.Name,
                        DocumentAlias = node.DocumentTypeAlias,
                        Children = node.Children.Select(x => GetNode(x)).ToList()
                    };
            }
        }

        private List<ArticleComponent> GetComponents(IPublishedContent node)
        {
            var controlsToExport = new[] { "downloads", "videoPicker", "linkBox", "creativeCommonsLicens" };
            var grid = JsonConvert.DeserializeObject<GridValue>(node.GetPropertyValue<string>("articleGrid"));
            var result = new List<ArticleComponent>();
            if(grid != null && grid.Sections != null)
            {
                foreach (var section in grid.Sections)
            {
                foreach (var row in section.Rows)
                {
                    foreach (var area in row.Areas)
                    {
                        foreach (var control in area.Controls)
                        {
                            var alias = control.Editor.Alias;
                            if (controlsToExport.Contains(alias))
                            { 
                                var value = JsonConvert.DeserializeObject<dynamic>(control.Value.ToString());
                                switch (alias)
                                {
                                    case "downloads":
                                        result.Add(DownloadsComponent.Convert(value));
                                        break;
                                    case "videoPicker":
                                        result.Add(VideoComponent.Convert(value));
                                        break;
                                    case "linkBox":
                                        result.Add(LinkBoxComponent.Convert(value));
                                        break;
                                    case "creativeCommonsLicens":
                                        result.Add(CreativeCommonsComponent.Convert(value));
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            }

            return result;
        }

        public class ExportResponse
        {
            public int Count { get; set; }
            public ExportItem Items { get; set; }
        }

        public class ExportItem
        {
            public string Name { get; set; }
            public string DocumentAlias { get; set; }
            public List<ExportItem> Children { get; set; }
        }

        private class ArticleExportItem : ExportItem
        {
            public int Id { get; set; }
            public string Title { get; set; }
            public string Summary { get; set; }
            public string OffentliggjortMedie { get; set; }
            public string ImageId { get; set; }
            public string ImageUrl { get; set; }
            public string Quote { get; set; }
            public string ImageByLine { get; set; }
            public DateTime Date { get; set; }
            public string Body { get; set; }
            public string[] Tags { get; set; }
            public string VideoId { get; set; }
            public List<ArticleComponent> Components { get; set; }
        }

        public abstract class ArticleComponent
        {
            private string Id { get; set; }
        }

        private class DownloadsComponent : ArticleComponent
        {
            [JsonProperty("header")]
            public string Header { get; set; }

            [JsonProperty("downloads")]
            public List<DownloadLink> Downloads { get; set; }

            public static DownloadsComponent Convert(dynamic controlValue)
            {
                var header = controlValue[0].header.value;
                var downloadLinks = new List<DownloadLink>();
                foreach (var link in controlValue[0].downloadLinks.value)
                {
                    if(!string.IsNullOrEmpty(link.file.Value))
                    {
                        Udi udi = Udi.Parse(link.file.Value);
                        IPublishedContent relativePath = _umbracoHelper.TypedMedia(udi);
                        downloadLinks.Add(new DownloadLink()
                        {
                            Caption = link.caption,
                            File = $"{_host}{relativePath.Url}"
                        });

                    }
                }
                return new DownloadsComponent()
                {
                    Header = header,
                    Downloads = downloadLinks
                };
            }
        }

        public class VideoComponent : ArticleComponent
        {
            [JsonProperty("startImage")]
            public string StartImage { get; set; }
            [JsonProperty("title")]
            public string Title { get; set; }
            [JsonProperty("videoId")]
            public string VideoId { get; set; }
            [JsonProperty("body")]
            public string Body { get; set; }

            public static VideoComponent Convert(dynamic controlValue)
            {
                var image = controlValue[0];
                return new VideoComponent()
                {
                    StartImage = image.startImage.value,
                    Title = image.title.value,
                    VideoId = image.videoId.value,
                    Body = image.body.value
                };
            }
        }

        private class LinkBoxComponent : ArticleComponent
        {
            public string Title { get; set; }
            public bool IsBackgroundColorWhite { get; set; }
            public List<Link> Links { get; set; }

            public static LinkBoxComponent Convert(dynamic controlValue)
            {
                var linkBox = controlValue[0];
                var links = new List<Link>();
                foreach (var link in linkBox.links.value) 
                {
                    var absoluteLink = link.link.Value;
                    if(!string.IsNullOrEmpty(absoluteLink))
                    {
                        if(link.link.Value.StartsWith("umb"))
                        {
                            var udi = Udi.Parse(link.link.Value);
                            var content = _umbracoHelper.TypedContent(udi);
                            absoluteLink = content != null ? content.Url : string.Empty;
                        }

                        links.Add(new Link()
                        {
                            Caption = link.caption,
                            OpenInNewWindow = link.newWindow,
                            Value = absoluteLink
                        });
                    }
                }
                return new LinkBoxComponent()
                {
                    Title = linkBox.title.value,
                    Links = links
                };
            }
        }

        private class Link
        {
            public string Caption { get; set; }
            public string Value { get; set; }
            public bool OpenInNewWindow { get; set; }
        }

        private class DownloadLink
        {
            public string Caption { get; set; }
            public string File { get; set; }
        }

        public class CreativeCommonsComponent : ArticleComponent
        {
            public string Value { get => "Used here"; }
            public static CreativeCommonsComponent Convert(dynamic controlValue)
            {
                return new CreativeCommonsComponent();
            }
        }
    }
}
