﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace Trasportministeriet.Core.Controllers.api
{
    public class SortPublicationsController : UmbracoApiController
    {
        [HttpPost]
        [Route("api/[controller]")]
        public object Sort(string year, int id)
        {
          
            var publications = Umbraco.TypedContent(id);
            var publicationsToSend = new List<IPublishedContent>();            
            var response = new List<object>();


            var SortedPublications = publications.Descendants().Where(x => x.IsVisible() && x.DocumentTypeAlias == "articlePage").Where(x=>x.GetPropertyValue<DateTime>("date").ToString("yyyy").Equals(year)).OrderBy(x => x.GetPropertyValue<DateTime>("date")).Reverse();


            foreach (var publication in SortedPublications)
            {

                var result = new Dictionary<string, object>();

                var img = publication.GetPropertyValue<IPublishedContent>("image");
                var date = publication.GetPropertyValue<DateTime>("date").ToString("dd. MMMM yyyy");
                var title = publication.GetPropertyValue<string>("title");

                if (img!=null)
                {
                  result.Add("Img", img.Url);
                }
                else
                {
                    result.Add("Img", string.Empty);
                }
                result.Add("Date",date);
                result.Add("Title", title);
                result.Add("Url", publication.Url);

                response.Add(result);

            }




            return response.Take(12);
        }
    }
}
