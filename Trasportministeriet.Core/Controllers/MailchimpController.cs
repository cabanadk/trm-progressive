﻿using System.Collections.Generic;
using System.Web.Mvc;
using Trasportministeriet.Core.Models;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using System;

namespace Trasportministeriet.Core.Controllers
{
    public class ListModel
    {
        public string list { get; set; }
        public string permision { get; set; }
    }
    public class MailchimpController : SurfaceController
    {
        [HttpGet]
        public ActionResult AddToList(string email, int pageId, string interests)
        {

            List<string> lists = interests.Split('|').ToList();
            NewsletterViewModel viewList = new NewsletterViewModel();
            if (lists != null && lists.Count() > 0)
            {
                var cs = ApplicationContext.Services.ContentService;
                var content = Umbraco.Content(pageId);
                if (content != null)
                {
                    IPublishedContent root = content.AncestorOrSelf(1);
                    

                    var hashEmail = string.IsNullOrEmpty(email) ? "" : MD5Hash(email.ToLower());

                    string apikey = root.GetPropertyValue("mailchimpApikey").ToString();
                    var dc = apikey.Split('-')[1];
                    var count = 0;
                    foreach (var list in lists)
                    {
                        List<string> items = list.Split(',').ToList();

                        if (items.Count() == 2)
                        {
                            string mailchimpurl = string.Format("https://{0}.api.mailchimp.com/3.0/lists/{1}/members/{2}", dc, items[0], hashEmail);
                            var member = JsonConvert.SerializeObject(
                                new
                                {
                                    email_address = email,
                                    status = "pending",
                                    marketing_permissions = new Object[] { new { marketing_permission_id = items[1], text = "Email", enabled = true } }
                                }
                            );
                            try
                            {
                                using (var webClient = new WebClient())
                                {
                                    webClient.Headers.Add("Accept", "application/json");
                                    webClient.Headers.Add("Authorization", "apikey " + apikey);
                                    try
                                    {
                                        string result = webClient.UploadString(mailchimpurl, "PUT", member);
                                    }
                                    catch (WebException we)
                                    {
                                        using (var sr = new StreamReader(we.Response.GetResponseStream()))
                                        {
                                            viewList.Description = we.ToString();
                                            return PartialView("~/Views/Partials/newsletter/response.cshtml", viewList);
                                        }
                                    }
                                    count++;
                                }
                                if (count == lists.Count())
                                {
                                    var msg = content.GetPropertyValue("successMessage").ToString();
                                    viewList.Description = msg;
                                    return PartialView("~/Views/Partials/newsletter/response.cshtml", viewList);
                                }
                            }
                            catch (WebException we)
                            {
                                using (var sr = new StreamReader(we.Response.GetResponseStream()))
                                {
                                    viewList.Description = content.GetPropertyValue("errorMessage").ToString();
                                    return PartialView("~/Views/Partials/newsletter/response.cshtml", viewList);
                                }
                            }
                        }
                        
                    }
                }
                viewList.Description = content.GetPropertyValue("errorMessage").ToString();
            }
            return PartialView("~/Views/Partials/newsletter/response.cshtml", viewList);
        }
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }
}