﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;

namespace Trasportministeriet.Core.Controllers
{
    public class RenderPageSurfaceController: SurfaceController
    {
        public ActionResult RenderPage(int NodeId=0)
        {
            var Page = Umbraco.TypedContent(1125);
            return Redirect(Page.Url);
        }
    }
}
