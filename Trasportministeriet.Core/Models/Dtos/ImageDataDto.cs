﻿namespace Trasportministeriet.Core.Models.Dtos
{
    public class ImageDataDto
    {
        public ImageDataDto(string imageUrl)
        {
            ImageUrl = imageUrl;
        }
        public string ImageUrl { get; set; }
        public string AlternateText { get; set; }
        public string CaptionText { get; set; }
        public string ImageType { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
    }
}
